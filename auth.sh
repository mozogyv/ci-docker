#!/bin/sh
set -eu

# Docker JSON configuration for both GitLab container registry and Amazon ECR
# User home path as first argument

mkdir -pv "${1}/.docker"
CI_REGISTRY_AUTH=$(echo -n "${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')
echo "{\"auths\":{\"registry.gitlab.com\":{\"auth\":\"${CI_REGISTRY_AUTH}\"}},\"credHelpers\":{\"${AWS_ECR}\":\"ecr-login\"}}" > "${1}/.docker/config.json"
