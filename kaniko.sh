#!/bin/sh
set -eu

./auth.sh '/kaniko'

set -x

/kaniko/executor --context="${CI_PROJECT_DIR}" \
                 --dockerfile="${CI_PROJECT_DIR}/Dockerfile" \
                 --destination="${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}-${TARGETARCH}" \
                 --destination="${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}-${TARGETARCH}" \
                 --build-arg=DOCKER_VERSION \
                 --build-arg=TARGETARCH \
                 --build-arg=TARGETARCHALT
