#!/bin/sh
set -eu

# Merge docker images of different architectures into a single tag

./auth.sh '/root'

set -x

# GitLab container registry
docker manifest create "${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}" \
               --amend "${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}-amd64" \
               --amend "${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}-arm64"
docker manifest push   "${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}"

# AWS ECR
docker manifest create "${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}" \
               --amend "${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}-amd64" \
               --amend "${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}-arm64"
docker manifest push   "${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}"
