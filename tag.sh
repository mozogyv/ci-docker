#!/bin/sh
set -eu

# Compute common Docker image tag

if [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]
then
  DOCKER_IMAGE_TAG='latest'
elif [ -n "${CI_COMMIT_TAG:-}" ]
then
  DOCKER_IMAGE_TAG="${CI_COMMIT_TAG}"
else
  DOCKER_IMAGE_TAG="${CI_COMMIT_REF_NAME}"
fi

echo "DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG}" > .env
